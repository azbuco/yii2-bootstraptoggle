# Bootstrap Toggle widget for Yii 2 #

This extension provides the [Bootstrap-toggle](http://www.bootstraptoggle.com) integration for the Yii2 framework.

## Usage ##

```php
use azbuco\bootstraptoggle\Toggle;

Toggle::widget([
    'name' => 'foo',
    'checked' => true,
    'options' => [],
])
```
or 

```php
<?= $form->field($model, 'status')->widget(Toggle::className(), [
    'options' => []
]); ?>
```
You can configure the plugin with data options [see here](http://www.bootstraptoggle.com)
