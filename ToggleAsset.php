<?php

namespace azbuco\bootstraptoggle;

use yii\web\AssetBundle;

class ToggleAsset extends AssetBundle
{

    public $sourcePath = '@bower/bootstrap-toggle';
    public $js = [
        'js/bootstrap-toggle.min.js'
    ];
    public $css = [
        'css/bootstrap-toggle.min.css'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];

}
