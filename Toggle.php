<?php

namespace azbuco\bootstraptoggle;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\web\View;
use yii\widgets\InputWidget;

/**
 * Wrapper for the Bootstrap Toggle javascript pulgin, that converts checkboxes to toggles
 * @see http://www.bootstraptoggle.com
 * Usage:
 * 
 * ```
 * <?= \azbuco\bootstraptoggle\Toggle::widget([
 *      'name' => 'Toggle',
 *      'options' => [
 *          'data-size' => 'large',
 *      ]
 *  ]);?>
 * ```
 */
class Toggle extends InputWidget
{

    /**
     * @var bool default state of the toggle if no model defined
     */
    public $checked = false;

    /**
     * @var string label for the input 
     */
    public $label = '';

    /**
     * @var array the options for the Bootstrap Toogle plugin
     */
    public $options = [];

    /**
     * @var array the default options for the Bootstrap Toggle widget
     */
    public $defaultOptions = [
        'data-toggle' => 'toggle',
        'data-size' => 'mini',
        'label' => false,
    ];

    /**
     *
     * @var string bootstrap layout for the input
     */
    public $layout = '<div class="checkbox bootstraptoggle">{input}<label>{label}</label></div>';

    /**
     * @var View the current view
     */
    protected $_view;

    /**
     * @var array the event handlers for the underlying Bootstrap Toggle input JS plugin.
     * @see http://www.bootstraptoggle.com/#events
     */
    public $clientEvents = [];

    public function init()
    {
        $this->_view = $this->getView();
    }

    public function run()
    {
        $this->registerBundle();
        $this->registerClientScript();

        return $this->renderLayout();
    }

    /**
     * Registers plugin and the related events
     */
    protected function registerBundle()
    {
        ToggleAsset::register($this->_view);
        // fix incorrect css in plugin
        $this->_view->registerCss('.checkbox .toggle.btn-xs label:first-of-type { padding-left: 12px; }');
    }

    /**
     * Register JS
     */
    protected function registerClientScript()
    {
        $id = $this->getId();

        $js[] = ';'; // ';$("#' . $id . '").bootstrapToggle();";
        foreach ($this->clientEvents as $event => $handler) {
            $js[] = '$(document).on("' . $event . '", "#' . $id . '", ' . $handler . ');';
        }
        $this->_view->registerJs(implode("\n", $js));
    }

    protected function renderLayout()
    {
        $content = preg_replace_callback("/{(\\w+)}/", function ($matches) {
            $renderMethod = 'render' . Inflector::id2camel($matches[1]);
            if (!method_exists($this, $renderMethod)) {
                return $matches[0];
            }
            return $this->$renderMethod();
        }, $this->layout);



        return $content;
    }

    protected function renderInput()
    {
        $options = ArrayHelper::merge($this->defaultOptions, $this->options);

        if ($this->hasModel()) {
            return Html::activeCheckbox($this->model, $this->attribute, $options);
        }
        return Html::checkbox($this->name, $this->checked, $options);
    }

    protected function renderLabel()
    {
        return $this->label;
    }

}
